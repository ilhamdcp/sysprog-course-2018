#!/bin/bash
pid2="$(ps aux | grep '[s]inger2.sh' | awk -F: 'BEGIN{ FS="[ ]+" } { print $2 }')"
lyrics="lyrics.txt"
readLyrics()
{
	if [[ $pid2 == '' ]]
	then
		pid2="$(ps aux | grep '[s]inger2.sh' | awk -F: 'BEGIN{ FS="[ ]+" } { print $2 }')"
	fi
	while IFS= read -r var 
	do
		if [[ ${var/'2'} = $var ]]
		then
			echo ${var/'1$'/''}
		else
			echo $var > part
			kill -USR1 $pid2
			echo '...'
		fi
		sleep 1
	done < "$lyrics"
}


trap "readLyrics" TERM USR1
if [[ $pid2 == '' ]]
then
	while [ 1 -gt 0 ]
	do
		echo ...
		sleep 1
	done
fi
