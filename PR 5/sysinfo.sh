var=''

systemStatus()
{
    echo '-------------------------'
    echo '     System Status       '
    echo '-------------------------'
    echo 'Username : ' $(uname)
    echo 'OS : '$(uname; uname -r)
    echo "Uptime : " $(uptime | awk -F'( |:(user))+' '{print $4 " "$5}')
    echo 'IP :' $(ip address | awk '/inet /{print $2}' | tail)
    echo 'Hostname :' $(hostname)
}

hardwareList()
{
    echo '-------------------------'
    echo '     Hardware List       '
    echo '-------------------------'
    echo 'Architecture : ' $(lscpu | grep '^Architecture[ ]*' | awk 'BEGIN { FS = "[ ]+" } { print $2 }')
    echo "$(lshw -short)"
}

memory()
{
    echo '-------------------------'
    echo '        Memory           '
    echo '-------------------------'
    echo '*************************'
    echo '        Memory           '
    echo '*************************'
    echo 'Size : ' $(free --mega | grep 'Mem:' | awk 'BEGIN { FS = "[ ]+" } {print $2+$3}') 'MB'
    echo 'Free : ' $(free --mega | grep 'Mem:' | awk 'BEGIN { FS = "[ ]+" } {print $3}') 'MB'
    echo '*************************'
    echo '    Memory Statistics    '
    echo '*************************'
    vmstat
    echo '*********************************'
    echo '    Top 10 cpu eating process    '
    echo '*********************************'
    ps -eo user,pid,%cpu,%mem,vsz,rss,tty,stat,start,time,command --sort=-%cpu | head -n 10

}

hardwareDetail()
{
    echo '========================='
    echo '     Hardware Detail     '
    echo '========================='
    echo '1. CPU'
    echo '2. BLock Devices'
    echo '3. Back'
    printf 'Choose 1-3 : '
    read hardware
    if [ $hardware -eq '1' ]
    then
        echo '-------------------------'
        echo '          CPU            '
        echo '-------------------------'
        echo 'Model name : ' $(lscpu | grep 'Model name:' |  awk 'BEGIN { FS = "(   )+" } { print $2 }')
        echo 'Frequency : '$(lscpu | grep 'CPU MHz:' | awk 'BEGIN { FS = "[: ]+" } { print $3 }')
        echo 'Cache : '$(lscpu | grep 'L3 cache:' | awk 'BEGIN { FS = "[: ]+" } { print $3 }')
        var='0'
    elif [ $hardware -eq '2' ]
    then
        echo '-------------------------'
        echo '          blk            '
        echo '-------------------------'
        lsblk | grep 'sda'
        var='0'
    elif [ $hardware -eq '3' ]
    then
        var='3'
    fi
    
}

TZ=GMT-7 date
echo '===================='
echo '     Main Menu      '
echo '===================='
echo '1. Operating System Info'
echo '2. Hardware List'
echo '3. Free and Used Memory'
echo '4. Hardware Detail'
echo '5. exit'
printf 'Choose 1-5: '
read value

while [ $value -ne '5' ]
do
if [ $value -eq '1' ]
then
    systemStatus
elif [ $value -eq '2' ]
then
    hardwareList
elif [ $value -eq '3' ]
then
    memory
elif [ $value -eq '4' ]
then
    hardwareDetail
fi
if [[ $var -eq '3' ]]
then
    TZ=GMT-7 date
    echo '===================='
    echo '     Main Menu      '
    echo '===================='
    echo '1. Operating System Info'
    echo '2. Hardware List'
    echo '3. Free and Used Memory'
    echo '4. Hardware Detail'
    echo '5. exit'
    printf 'Choose 1-5: '
else
    printf 'Press [Enter] key to continue...'
    read -s -n 1 key
    if [[ $key = '' ]]
    then
        echo ''
        TZ=GMT-7 date
        echo '===================='
        echo '     Main Menu      '
        echo '===================='
        echo '1. Operating System Info'
        echo '2. Hardware List'
        echo '3. Free and Used Memory'
        echo '4. Hardware Detail'
        echo '5. exit'
        printf 'Choose 1-5: '
    fi
fi
var='0'
read value
done

echo 'Bye Bye....'
