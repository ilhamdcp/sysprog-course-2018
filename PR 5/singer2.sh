#!/bin/bash
pid1="$(ps aux | grep '[s]inger1.sh' | awk -F: 'BEGIN{ FS="[ ]+" } { print $2 }')"
lyrics="lyrics.txt"

readLyrics()
{
	if [[ $pid1 == '' ]]
	then
		pid1="$(ps aux | grep '[s]inger1.sh' | awk -F: 'BEGIN{ FS="[ ]+" } { print $2 }')"
	fi
	read -r var < part
	echo ${var/'2$'/''}
	sleep 1
}

trap 'readLyrics' TERM USR1
if [[ $pid1 == '' ]]
then
	while [ 1 -gt 0 ]
	do
		echo ...
		sleep 1
	done
else
kill -USR1 $pid1
while [ 1 -gt 0 ]
	do
		echo ...
		sleep 1
	done
fi
