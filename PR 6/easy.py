import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)

# setup pin for green LED
GPIO.setup(11, GPIO.OUT)
GPIO.setup(13, GPIO.OUT)
GPIO.setup(15, GPIO.OUT)

# setup pin for red LED
GPIO.setup(19, GPIO.OUT)
GPIO.setup(21, GPIO.OUT)
GPIO.setup(23, GPIO.OUT)

# function for checking input value
def isPrime(value, divident):
	if value <= 1 or divident < 1:
		return False
	elif divident > 1:
		return value % divident != 0 and isPrime(value, divident-1)
	else:
		return True

def isOdd(value):
	return value % 2 != 0

def isEven(value):
	return value % 2 == 0

def blink(pin):
	GPIO.output(pin, GPIO.HIGH)

def blinkoff(pin):
	GPIO.output(pin, GPIO.LOW)

# function to manipulate LED
def redBlink():
	for x in 11,13,15:
		blink(x)
		time.sleep(1)
		blinkoff(x)
		time.sleep(1)

def greenBlink():
	for x in 19,21,23:
		blink(x)
		time.sleep(1)
		blinkoff(x)
		time.sleep(1)	

def allBlink():
	for i in range(0,2):
		blink(11)
		blink(13)
		blink(15)
		time.sleep(1)
		blinkoff(11)
		blinkoff(13)
		blinkoff(15)
		time.sleep(1)
		blink(19)
		blink(21)
		blink(23)
		time.sleep(1)
		blinkoff(19)
		blinkoff(21)
		blinkoff(23)
		time.sleep(1)


# input receive
inputText = input('input number: ')

while inputText != '':
	if isPrime(int(inputText), int(inputText)-1):
		allBlink()
	elif isOdd(int(inputText)):
		redBlink()
	elif isEven(int(inputText)):
		greenBlink()
	inputText = input('input number: ')


GPIO.cleanup()