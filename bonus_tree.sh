#!/bin/bash
olddir=$PWD;
declare -i dirdepth=0;
totalDirectories=1;
function listfiles {
        cd "$1";
        for file in *
        do
                ## Print directories with brackets ([directory])
                if [ -d "$file" ]
                then
			for ((i=0; $i < $dirdepth; i++))
                	do
                        ##Tab between each level
                        printf "| ";
                	done
                        printf "| +~~~$file\n";
			((totalDirectories+=1));
                else
                        printf "";
                fi

                ##Work our way thru the system recursively
                if [ -d "$file" ]
                then
                        dirdepth=$dirdepth+1;
                        listfiles "$file";
                        cd ..;
                fi
        done
        ##Done with this directory - moving on to next file
        let dirdepth=$dirdepth-1;
}
printf "Initial Directory = $PWD\n"
printf "+~~~${PWD##*/}\n"
listfiles "$1";
##Go back to where we started
cd $olddir;
unset i dirdepth;
printf "Total directories = $totalDirectories\n"

