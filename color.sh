#!/bin/bash

# Script to edit PS1 shell variable to change the layout and coloring.
# This script can only work if you copy-paste it to the bash.

export PS1="\[\e[31m\][ \[\e[m\]\[\e[33m\]\u\[\e[m\]\[\e[32m\]@\[\e[m\]\[\e[34m\]\h\[\e[m\]:\[\e[35m\]\w\[\e[m\]\[\e[31m\] ]\[\e[m\]\[\e[32;47m\]\[\e[m\] bash-\v \[\e[96m\]\d\[\e[m\] \[\e[96m\]\@\[\e[m\]\n∞>"

